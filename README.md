# Org Chart / Phone Book

Some org-chart related programs.

The NRAO LDAP directory has photos and can be used to derive the management org chart. This is useful because the org 
chart is mostly recited ritually and not seen.

To run the web application, set up a conda environment and run it with flask:

    conda env create
    conda activate orgchart
    env FLASK_ENV=development FLASK_DEBUG=1 FLASK_APP=interactive.py flask run
