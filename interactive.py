from pathlib import Path
from typing import List, Optional, Set
from pycapo import CapoConfig

from flask import Flask, Response, redirect, url_for, render_template, request
from ldap3 import Server, Connection, ALL
from ldap3.core.exceptions import LDAPCommunicationError

app = Flask(__name__)

Username = str


class Reconnector:
    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs
        self.conn = self.reconnect()

    def reconnect(self) -> Connection:
        return Connection(*self.args, **self.kwargs)

    def search(self, *args, **kwargs):
        try:
            return self.conn.search(*args, **kwargs)
        except LDAPCommunicationError:
            self.conn = self.reconnect()
            return self.conn.search(*args, **kwargs)

    @property
    def entries(self):
        return self.conn.entries


class Phonebook:
    """
    Abstract Phonebook for looking up people.
    """
    _STANDARD_ATTRIBUTES = ['cn', 'extensionAttribute2', 'sAMAccountName', 'department', 'description', 'title',
                            'roomNumber', 'ou', 'st', 'telephoneNumber', 'employeeNumber', 'employeeType', 'homePhone',
                            'memberOf']
    _USER_SEARCH_BASE = "OU=ADUsersContacts,DC=ad,DC=nrao,DC=edu"
    _GROUP_SEARCH_BASE = "OU=NRAOGroups,DC=ad,DC=nrao,DC=edu"

    def __init__(self):
        config = CapoConfig().settings('edu.nrao.ldap')
        user = config.username
        password = config.password

        server = Server(config.hostname, get_info=ALL)
        self.conn = Reconnector(server, user, password, auto_bind=True, pool_size=1, pool_lifetime=300)
        self._groups = {}

    def lookup_person(self, name : Username) -> Optional["Person"]:
        """
        Look up a person by their 8-character account name
        :param name: account name (i.e. dlyons)
        :return: Person instance
        """
        self.conn.search(self._USER_SEARCH_BASE,
                    f"(&(objectClass=user)(sAMAccountName={name}))",
                         attributes=self._STANDARD_ATTRIBUTES)

        if self.conn.entries:
            record = self.conn.entries[0]
            return Person(record)
        else:
            return None

    def search(self, search_str):
        """
        search for a person by a string
        :param search_str:   the string to search for
        :return:  List of Person instances
        """
        phonebook.conn.search(phonebook._USER_SEARCH_BASE,
                              f"(&(objectClass=user)(sAMAccountName={search_str}))",
                              attributes=['*'])
        records = []
        for entry in self.conn.entries:
            records.append(Person(entry))

        return records

    def lookup_subordinates(self, person : "Person") -> List["Person"]:
        self.conn.search(self._USER_SEARCH_BASE,
                         f"(&(objectClass=user)(extensionAttribute2={person.username}))",
                         attributes=self._STANDARD_ATTRIBUTES)

        subordinates = []
        for record in self.conn.entries:
            subordinates.append(Person(record))
        return subordinates

    def lookup_peers(self, person : "Person") -> List["Person"]:
        peers = []
        if person.manager:
            self.conn.search(self._USER_SEARCH_BASE,
                             f"(&(objectClass=user)(extensionAttribute2={person.manager.username}))",
                             attributes=self._STANDARD_ATTRIBUTES)

            for record in self.conn.entries:
                if Person(record).username != person.username:
                    peers.append(Person(record))
        return peers

    def lookup_image_for(self, name : Username) -> bytes:
        """
        Return the JPEG content for a person's picture.

        :param name: Username of person
        :return: JPEG image for that person
        """

        # we do a little cache dance here, if we do not have the image already, look it up and cache it, then serve
        # up the cached data regardless
        cache_path = Path('cache') / (name + '.jpg')
        if not cache_path.exists():
            self.conn.search(self._USER_SEARCH_BASE,
                             f"(&(objectClass=user)(sAMAccountName={name}))",
                             attributes=['jpegPhoto'])
            cache_path.write_bytes(self.conn.entries[0].jpegPhoto.value)

        return cache_path.read_bytes()

    def lookup_departments(self) -> Set[str]:
        self.conn.search(self._USER_SEARCH_BASE, '(objectClass=user)', attributes=['department'])
        return set(record.department.value for record in self.conn.entries)

    def lookup_groups(self) -> List["Group"]:
        phonebook.conn.search(phonebook._GROUP_SEARCH_BASE, '(objectClass=group)', attributes=['cn', 'description', 'distinguishedName'])
        return [Group(entry) for entry in phonebook.conn.entries]

    @property
    def groups(self):
        if not self._groups:
            self._groups = dict((g.distinguishedName, g) for g in self.lookup_groups())
        return self._groups


phonebook = Phonebook()


class Person(object):
    """
    Represents a person who can be looked up in the phonebook.
    """
    def __init__(self, record):
        self.record = record

    def __getattr__(self, item):
        return getattr(self.record, item).value

    @property
    def fullname(self):
        return self.record.cn.value

    @property
    def username(self):
        return self.record.sAMAccountName.value

    @property
    def manager(self) -> "Person":
        return phonebook.lookup_person(self.record.extensionAttribute2.value)

    @property
    def room_number(self):
        return self.record.roomNumber.value

    @property
    def organizational_unit(self):
        return self.record.ou.value

    @property
    def location(self):
        if self.organizational_unit == 'CV':
            return 'Charlottesville'
        elif self.organizational_unit == 'CHE':
            return 'Chile'
        elif self.organizational_unit == 'SO':
            return 'Socorro'
        elif self.organizational_unit == 'GB':
            return 'Green Bank'
        else:
            return self.organizational_unit

    @property
    def state(self):
        return self.record.st.value

    @property
    def home_phone(self):
        return self.homePhone

    @property
    def office_phone(self):
        return self.telephoneNumber

    @property
    def email_address(self):
        return self.username + '@nrao.edu'

    @property
    def employee_number(self):
        return self.employeeNumber

    @property
    def subordinates(self):
        return phonebook.lookup_subordinates(self)

    @property
    def peers(self):
        return phonebook.lookup_peers(self)

    @property
    def url(self):
        return url_for('person', name=self.username)

    @property
    def image_url(self):
        return url_for('image', name=self.username)

    @property
    def groups(self):
        return [phonebook.groups[g] for g in self.memberOf if g in phonebook.groups.keys()]


@app.route("/")
def hello():
    return redirect(url_for('person', name='tbeasley'))


@app.route("/<name>")
def person(name):
    person = phonebook.lookup_person(name)
    return render_template('person.html', person=person)


@app.route("/<name>/image.jpg")
def image(name):
    img = phonebook.lookup_image_for(name)
    if img is None:
        return f"No image for user {name}", 404
    else:
        return Response(img, content_type="image/jpeg")


@app.route("/departments")
def departments():
    return '<ul>' + ''.join('<li>' + i + '</li>' for i in phonebook.lookup_departments() if i is not None) + '</ul>'

@app.route("/search")
def search():
    search_str = request.args.get('search')
    records = phonebook.search(search_str)
    return render_template('search.html', search=search_str, records=records)


@app.route('/debug')
def everything():
    phonebook.conn.search(phonebook._USER_SEARCH_BASE,
                     f"(&(objectClass=user)(sAMAccountName=dlyons))",
                          attributes=['*'])
    record = phonebook.conn.entries[0]
    return render_template('everything.html', record=record.entry_attributes_as_dict)


class Group:
    def __init__(self, record):
        self.record = record

    def __getattr__(self, item):
        return getattr(self.record, item).value

    @property
    def name(self):
        print(self.record.entry_to_ldif())
        return self.cn

