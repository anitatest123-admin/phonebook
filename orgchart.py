from pycapo import CapoConfig
from ldap3 import Server, Connection, ALL

config = CapoConfig().settings('edu.nrao.ldap')
user = config.username
password = config.password

server = Server(config.hostname, get_info=ALL)
conn = Connection(server, user, password, auto_bind=True)

conn.search("OU=ADUsersContacts,DC=ad,DC=nrao,DC=edu","(objectClass=user)",  attributes = ['cn','extensionAttribute2','sAMAccountName','ou'])

nameToAccount = {}
userToManager = {}
for record in conn.entries:
    # the following line can be used to limit to just one site or the others
    # if record.ou.value != 'SO': continue
    
    # we need a couple maps here to do this properly
    # first, we need to hold onto the account name by the user's name,
    # so that we can handle cases where the extensionAttribute2 is a
    # full name rather than an account name
    #
    # second, we need to build up the associations between this user
    # and their manager. we retain this separately so that we can build
    # the final result after cleaning up the names
    
    # get the names
    fullname = record.cn.value
    username = record.sAMAccountName.value
    
    nameToAccount[fullname] = username
    
    # get the manager, if applicable (should only fail once on acohen)
    if record.extensionAttribute2.value is not None:
        manager = record.extensionAttribute2.value
        userToManager[username] = manager

# Now I want to preload the following folks who apparently do not go by their
# cn attribute in other people's management hierarchy
nameToAccount['Tony Remijan'] = 'aremijan'
nameToAccount['Ken Kellerman'] = 'kkellerm'
nameToAccount["Karen O'Neil"] = 'koneil'
nameToAccount['Emonts'] = 'bemonts'
nameToAccount['Yoon'] = 'iyoon'
nameToAccount['Pat Murphy'] = 'pmurphy'
nameToAccount['Loreto Barcos-Munoz'] = 'lbarcos'
nameToAccount['Suzy Gurton'] = 'sgurton'

# The following person is a guest and their managers don't exist, so I'm
# removing them manually here
if 'ebehrens' in userToManager: del userToManager['ebehrens']

# OK, having gotten here, we should now have the maps we need to make the graph
# First, let's cleanup the userToManager dictionary
for user, manager in userToManager.items():
    if ' ' in manager:
        # first, let's try to split it up and clean it up a bit
        managers = [name.strip() for name in manager.replace('&','/').split('/')]

        # now let's try and replace them with their username if possible
        managers = [nameToAccount[name] if name in nameToAccount else name
                    for name in managers]

        userToManager[user] = managers
    else:
        userToManager[user] = [manager]

# Now output the graph in Graphviz format        
print('digraph G {')
for user, managers in userToManager.items():
    for manager in managers:
        print(f'  {user} -> {manager}')
print('}')

# From here, it's on you to run dot on it, but you probably want this:
#
#   dot -Tpdf -o orgchart.pdf orgchart.dot
#
# assuming you wrote the output of this script to orgchart.dot
